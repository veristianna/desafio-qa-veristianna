  # Language: pt
  Feature: Receber mensagens

    Como usuário do WhatsApp
    Eu quero pode receber mensagens
    Para que possa me comunicar com os demais usuários do aplicativo
	
	Scenario: Receber mensagem com smartphone no modo avião ativado
      Given Smartphone estiver com modo avião ativado
	   And Usuário ter acessado aplicativo
      When Mensagem do aplicativo WhatsApp for enviada para usuário sem sinal 
	  Then Mensagem não é recebida pelo aplicativo
	
	Scenario: Verificar que mensagem é recebida após desativar modo avião
      Given Smartphone estiver com modo avião ativado
	   And Ter notificações de novas mensagens ativadas para o aplicativo
	  When Usuário desativar modo avião no smartphoje 
       And Usuário acessar o aplicativo
	  Then Conversa será apresentada como não lida 
      But Não será apresentada mensagem de notificação de nova mensagem no smartphone
	
	Scenario: Receber mensagem quando estiver lendo outra mensagem do aplicativo
      Given Usuário estiver lendo uma mensagem recebida no aplicativo
	   And Ter notificações de novas mensagens ativadas para o aplicativo
	  When Aplicativo receber nova mensagem  
      Then Será apresentada notificação de nova mensagem no canto superior do smartphoje
        And Usuário clica na notificação de nova mensagem 
	  Then Aplicativo redirecionará para mensagem que foi recebida
	
	Scenario: Receber mensagem quando notificações de novas mensagens estiver desativa
      Given Ter notificações de novas mensagens desativadas para o 
	    And usuário estar na tela Conversas do aplicativo
	  When Aplicativo receber nova mensagem  
      Then Será apresentada nova mensagem como não lida
      But Não será apresentada notificação de novas mensagens no aplicativo no canto superior