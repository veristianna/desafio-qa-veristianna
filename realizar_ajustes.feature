  # Language: pt
  Feature: Realizar ajustes

    Como usuário do WhatsApp
    Eu quero realizar ajustes no aplicativo
    Para que aplicativo tenha as configurações personalizadas ao meu uso
	
	Scenario: Configurar para salvar vídeos e fotos no rolo de câmera do smartphone
      Given Usuário ter acessado aplicativo
	    And Usuário ter acessado opção Ajustes
      When Usuário seleciona opção Conversas
	    And Usuário ativa opção Salvar no Rolo de Câmera
	  Then Opção Salvar no Rolo de Câmera estará ativada
	
	Scenario: Verificar que fotos são salvas no rolo de camêra do smartphone
      Given Usário ter ativado no aplicativo opção Ajustes >> Conversas >> Salvar no Rolo de Câmera
	    And Smartphone apresentar sinal para receber mensgens de fotos e vídeos
		And Smartphone tem espaço para salvar vídeos e fotos
      When Quando usuário recebe mensagem com foto
	    And Usuário usuário acessa a mensagem
		And Usuário visualiza foto
		And Usuário sai do aplicativo
		And Usuário acessa opçãp Fotos no smartphone
		And Usuário acessa opção Rolo de Câmera
	  Then Foto recebida pela mensagem no WhatsApp está salva no rolo de câmera
	
	Scenario: Verificar que vídeos são salvos no rolo de camêra do smartphone
      Given Usário ter ativado no aplicativo opção Ajustes >> Conversas >> Salvar no Rolo de Câmera
	    And Smartphone apresentar sinal para receber mensgens de fotos e vídeos
		And Smartphone tem espaço para salvar vídeos e fotos
      When Quando usuário recebe mensagem com vídeo
	    And Usuário usuário acessa a mensagem
		And Usuário visualiza vídeo
		And Usuário sai do aplicativo
		And Usuário acessa opçãp Fotos no smartphone
		And Usuário acessa opção Rolo de Câmera
	  Then Vídeo recebido pela mensagem no WhatsApp está salvo no rolo de câmera
	
	Scenario: Configurar para não salvar vídeos e fotos no rolo de câmera do smartphone
      Given Usuário ter acessado aplicativo
	    And Usuário ter acessado opção Ajustes
      When Usuário seleciona opção Conversas
	    And Usuário desativa opção Salvar no Rolo de Câmera
	  Then Opção Salvar no Rolo de Câmera estará deativada
	
	Scenario: Verificar que fotos não são salvas no rolo de camêra do smartphone
      Given Usário ter desativado no aplicativo opção Ajustes >> Conversas >> Salvar no Rolo de Câmera
	    And Smartphone apresentar sinal para receber mensgens de fotos e 
	  When Quando usuário recebe mensagem com foto
	    And Usuário usuário acessa a mensagem
		And Usuário visualiza foto
		And Usuário sai do aplicativo
		And Usuário acessa opçãp Fotos no smartphone
		And Usuário acessa opção Rolo de Câmera
	  Then Foto recebida pela mensagem no WhatsApp não está salva no rolo de câmera
	
	Scenario: Verificar que vídeos não são salvos no rolo de camêra do smartphone
      Given Usário ter desativado no aplicativo opção Ajustes >> Conversas >> Salvar no Rolo de Câmera
	    And Smartphone apresentar sinal para receber mensgens de fotos e vídeos
	  When Quando usuário recebe mensagem com vídeo
	    And Usuário usuário acessa a mensagem
		And Usuário visualiza vídeo
		And Usuário sai do aplicativo
		And Usuário acessa opçãp Fotos no smartphone
		And Usuário acessa opção Rolo de Câmera
	Then Vídeo recebido pela mensagem no WhatsApp não mão está salvo no rolo de câmera
	
	Scenario: Verificar que usuário pode salvar fotos no rolo de camêra do smartphone
      Given Usário ter desativado no aplicativo opção Ajustes >> Conversas >> Salvar no Rolo de Câmera
	    And Smartphone apresentar sinal para receber mensgens de fotos e vídeos
		And Smartphone tem espaço para salvar vídeos e fotos
      When Quando usuário recebe mensagem com foto
	    And Usuário usuário acessa a mensagem
		And Usuário visualiza foto
		And Usuário selecionar foto
		And Usuário seleciona ícone quadrado com seta para cima no canto inferioresquerda
        And Usuário seleciona opção Salvar		
		And Usuário sai do aplicativo
		And Usuário acessa opçãp Fotos no smartphone
		And Usuário acessa opção Rolo de Câmera
	  Then Foto recebida pela mensagem no WhatsApp está salva no rolo de câmera
	
	Scenario: Verificar que usuário pode salvar vídeos no rolo de camêra do smartphone
      Given Usário ter desativado no aplicativo opção Ajustes >> Conversas >> Salvar no Rolo de Câmera
	    And Smartphone apresentar sinal para receber mensgens de fotos e vídeos
		And Smartphone tem espaço para salvar vídeos e fotos
      When Quando usuário recebe mensagem com foto
	    And Usuário usuário acessa a mensagem
		And Usuário visualiza vídeo
		And Usuário selecionar vídeo
		And Usuário seleciona ícone quadrado com seta para cima no canto inferioresquerda
        And Usuário seleciona opção Salvar		
		And Usuário sai do aplicativo
		And Usuário acessa opçãp Fotos no smartphone
		And Usuário acessa opção Rolo de Câmera
	  Then Vídeo recebida pela mensagem no WhatsApp está salva no rolo de câmera